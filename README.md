# plug5 (or formally high⁵)
> A lightweight and yet powerful plugin loader

## Why plug5?

#### Easy to use
Load your plugins with only one simple function call.

#### Extremely lightweight
The whole package is just 1.4 kB in total.

#### Blazing fast
Plug5 uses the standard `require` function to load it's files.
Querying and loading an example `node_modules` folder, containing `7.0 MB`, `66` packages in `1,826` files, only took `167.875ms`.

## Installation
```sh
yarn add plug5
```

## Usage
A plugin contains of:
- A folder (usually the name of the plugin)
- A plugin.json (similar to npm's `package.json`) containing the following fields
   - name: String, the name of the plugin, default: name of folder
   - version: String, the version of the plugin, default: '0.0.1'
   - main: String, entry point of the plugin, default: 'index.js'
- A JavaScript file (usually `index.js`; name gets specified in `plugin.json`)

The `loadPlugins` function returns an array of object, with the following fields:
- name: String, the name of the plugin
- version: String, the version of the plugin
- _: Function, the via `require` loaded code
```javascript
const loadPlugins = require('plug5')

const plugins = loadPlugins('./dir/')

/*

   [
      {
         name: 'Foo',
         version: '0.1.2',
         _: [Function: example]
      },
      ...
   ]

*/

```