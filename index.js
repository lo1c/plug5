const fs = require('fs-plus')
const path = require('path')
const fromDefault = require('from-default')

const defaultPlugin = {
	main : 'index.js'
}

function loadPlugins(pluginPath) {
	const plugins = []

	const files = fs.listSync(pluginPath)
	for (let folder of files) {
		folder = fs.absolute(folder)
		const basename = path.basename(folder)
		if (basename.startsWith('.')) continue

		if (fs.isDirectorySync(folder)) {
			let plugin = {}

			if (fs.existsSync(path.join(folder, 'plugin.json'))) {
				plugin = fromDefault(defaultPackage, JSON.parse(fs.readFileSync(path.join(folder, 'package.json'))))
			} else {
				plugin = {
					name    : basename,
					version : '0.0.1',
					main    : 'index.js'
				}
			}

			if (!fs.existsSync(path.join(folder, plugin.main))) continue
			// return Error(`Missing file: ${plugin.main} for ${plugin.name} (${folder})`)

			// if(!fs.existsSync(path.join(folder, plugin.main)))

			plugins.push({
				name    : plugin.name || basename,
				version : plugin.version || '0.0.1',
				_       : require(path.join(folder, plugin.main))
			})
		}
	}

	return plugins
}

module.exports = loadPlugins
